import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:parno_ui/src/constants.dart' as constants;

class LightTextButton extends StatelessWidget {
  LightTextButton(this.text, {@required this.onPressed});

  final String text;
  final void Function() onPressed;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: onPressed,
        child: Container(
          decoration: BoxDecoration(
            color: Colors.transparent,
          ),
          padding: EdgeInsets.all(constants.defaultPaddingLarge),
          child: Text(
            text,
            style: constants.lightButtonTextStyle,
          ),
        ));
  }
}
