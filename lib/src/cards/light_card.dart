import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:parno_ui/src/constants.dart' as constants;

class LightCard extends StatelessWidget {
  LightCard(
      {this.background = constants.defaultGrey,
      this.width,
      this.height,
      this.borderRadius = constants.borderRadiusLarge,
      this.padding = constants.defaultPaddingLarge,
      this.child});

  LightCard.small(
      {this.background = constants.defaultGrey,
      this.width,
      this.height,
      this.borderRadius = constants.borderRadiusLarge,
      this.padding = constants.defaultPaddingSmall,
      this.child});

  LightCard.text(String text,
      {this.background = constants.defaultGrey,
      this.width,
      this.height,
      this.borderRadius = constants.borderRadiusLarge,
      this.padding = constants.defaultPaddingSmall,
      TextStyle style})
      : this.child = Text(
          text,
          style: style != null
              ? style
              : TextStyle(
                  color: Colors.white,
                  fontSize: constants.mediumFontSize,
                ),
        );

  final Color background;
  final Widget child;
  final double width;
  final double height;
  final double borderRadius;
  final double padding;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: this.width,
      height: this.height,
      decoration: BoxDecoration(
        color: this.background,
        border: Border.all(
          width: 0.0,
          color: Colors.transparent,
        ),
        borderRadius: BorderRadius.all(Radius.circular(this.borderRadius)),
      ),
      child: Padding(padding: EdgeInsets.all(this.padding), child: this.child),
    );
  }
}
