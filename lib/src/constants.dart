import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

const double borderRadiusLarge = 28.0;

const double defaultPaddingSmall = 10.0;
const double defaultPaddingLarge = 15.0;

const double frostedBlurSigma = 18.0;

const double mediumFontSize = 16.0;

final defaultTextStyle = GoogleFonts.roboto(
  textStyle: TextStyle(
    fontSize: mediumFontSize,
  ),
);

final lightButtonTextStyle = GoogleFonts.roboto(
  textStyle: TextStyle(
    fontSize: mediumFontSize,
    fontWeight: FontWeight.bold,
    color: textButtonBlue,
  ),
);

const Color defaultGrey = const Color(0xFFDDDDDD);
const Color defaultTransparentGrey = const Color(0xADBCBCBC);
const Color textButtonBlue = const Color(0xFF3353E2);

Widget frostedFilter(Widget child) {
  final animationDuration = const Duration(milliseconds: 350);
  double begin = 0.0;
  double end = 1.0;
  Curve curve = Curves.decelerate;

  return Dialog(
    backgroundColor: Colors.transparent,
    elevation: 0,
    child: StatefulBuilder(builder: (context, setState) {
      return WillPopScope(
        onWillPop: () async {
          setState(() {
            final tmp = begin;
            begin = end;
            end = tmp;
            curve = curve.flipped;
          });

          await Future.delayed(animationDuration);
          return true;
        },
        child: TweenAnimationBuilder<double>(
          tween: Tween<double>(begin: begin, end: end),
          curve: curve,
          duration: animationDuration,
          builder: (context, value, child) {
            return BackdropFilter(
                filter: ImageFilter.blur(
                    sigmaX: value * frostedBlurSigma,
                    sigmaY: value * frostedBlurSigma),
                child: fadeAndSlideAnimationBuilder(
                    child: child, verticalOffset: 50.0, step: value));
          },
          child: child,
        ),
      );
    }),
  );
}

Widget fadeAndSlideAnimationBuilder(
    {@required Widget child,
    double verticalOffset = 0.0,
    double horizontalOffset = 0.0,
    @required double step}) {
  return Transform.translate(
    offset: Offset(
        horizontalOffset == 0.0
            ? 0.0
            : horizontalOffset * step - horizontalOffset,
        verticalOffset == 0.0 ? 0.0 : verticalOffset * step - verticalOffset),
    child: Opacity(
      opacity: step,
      child: child,
    ),
  );
}
