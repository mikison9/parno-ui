import 'package:flutter/material.dart';
import 'package:parno_ui/src/constants.dart' as constants;

Future<T> showFrostedDialog<T>(
    {@required BuildContext context, Widget child}) async {
  return showDialog<T>(
    context: context,
    child: constants.frostedFilter(child),
    barrierColor: Colors.transparent,
  );
}
