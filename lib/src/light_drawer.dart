import 'package:flutter/material.dart';

class LightDrawer extends StatelessWidget {
  LightDrawer({@required this.child});

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: this.child,
    );
  }
}
