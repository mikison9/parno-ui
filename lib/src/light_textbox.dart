import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:parno_ui/parno_ui.dart';
import 'package:parno_ui/src/constants.dart' as constants;

class LightTextBox extends StatelessWidget {
  LightTextBox(
      {this.icon, this.controller, this.width, this.obscureText = false});

  final Widget icon;
  final TextEditingController controller;
  final double width;
  final bool obscureText;

  @override
  Widget build(BuildContext context) {
    return LightCard.small(
      width: this.width,
      child: icon != null
          ? LayoutBuilder(builder: (context, constraints) {
              final double iconSize = 20.0;

              return Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    height: iconSize,
                    width: iconSize,
                    child: FittedBox(fit: BoxFit.fill, child: this.icon),
                  ),
                  SizedBox(
                    width: constraints.maxWidth - iconSize,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 7.0),
                      child: _buildTextField(),
                    ),
                  ),
                ],
              );
            })
          : _buildTextField(),
    );
  }

  Widget _buildTextField() {
    return TextField(
      maxLines: 1,
      obscureText: this.obscureText,
      decoration: InputDecoration(
        isCollapsed: true,
        border: InputBorder.none,
        focusedBorder: InputBorder.none,
        enabledBorder: InputBorder.none,
        errorBorder: InputBorder.none,
        disabledBorder: InputBorder.none,
      ),
      style: constants.defaultTextStyle,
      enabled: true,
      controller: this.controller,
    );
  }
}
