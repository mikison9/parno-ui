import 'package:flutter/material.dart';
import 'package:parno_ui/src/constants.dart' as constants;

Widget roundedImage(Widget image) {
  return ClipRRect(
    borderRadius: BorderRadius.circular(constants.borderRadiusLarge),
    child: image,
  );
}
